module Main where

import qualified MyLib (someFunc)
import System.IO (openFile, hGetLine, hPutStr, hClose, IOMode( ReadWriteMode ))

data Symbol = I | O
data State = On | Off
data FiniteAutomaton = FA {
  lang :: Language
  state :: State
  transition :: State -> Symbol -> State
  }

-- Finite state automaton.

main :: IO ()
main = do
  putStrLn "Finite state automaton"
